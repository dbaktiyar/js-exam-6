var API = 'http://146.185.154.90:8000/blog/dushebaev.baktiyar@gmail.com/'

function post(url, data) {
    return $.ajax({
        method: 'POST',
        url: url,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        data: data
    })
}

function get(url) {
    return $.ajax({
        method: 'GET',
        url: url
    })
}

function renderPosts(posts) {
    var $postTemplate = $('#post-template');
    var $postsWrap = $('#posts');

    $postsWrap.text('')
    posts.forEach(post => {
        var template = $($($postTemplate).html()).clone()
        $(template).find('.mdl-card__supporting-text').append(`<p>${post.user.firstName} ${post.user.lastName}</p>`);
    $(template).find('.mdl-card__supporting-text').append(`<h4>${post.message}</h4>`);
    $($postsWrap).append(template);
})
}

function handleGettingPosts() {
    get(`${API}posts`)
        .then(function(res) {
            renderPosts(res);
        })
        .catch(function(err) {
            console.log(err);
        })
}

function initAddingPost() {
    var $form = $('#status-form')
    $form.on('submit', function(e) {
        e.preventDefault()
        var message = $form.find('input').val()
        post(`${API}posts`, {message: message})
            .then(function(res) {
                handleGettingPosts()
                alert('Your message added!')
            })
            .catch(function(err) {
                console.log(err);
            });
    });
}

function getProfileInfo() {
    get(`${API}profile`)
        .then(function(res) {
            $('#profile-name').text(`${res.firstName} ${res.lastName}`);
        });
}



function init() {
    getProfileInfo()
    handleGettingPosts()
    initAddingPost()
}

init()
